# SMB/Share
This section will go over how to set up SMB/Share Sharing

## Install
```
sudo apt install samba samba-common-bin
```
## Users
- Setup A User For Samba
    ```
    sudo useradd sambauser //Create user
    sudo passwd sambauser //Setup regular unix password
    sudo smbpasswd -a sambauser //Setup samba password
    ```

## Configure
- Open Configuration file
    ```
    sudo vim /etc/samba/smb.conf
    ```
- Append these lines to the bottom
    ```
    [SambaShare]
    comment = "/Share driectory share"
    path = /Share
    read only = no
    writeable = yes
    browseable = yes
    valid users = sambauser zireael9797
    create mask = 0775
    directory mask = 0775
    public = no
    ```

- Restart
    ```
    sudo /etc/init.d/smbd restart
    ```
    ```
    sudo /etc/init.d/nmbd restart
    ```
    