# Systemd Commands

## Check services
```
systemctl list-units
```
```
systemctl list-units --all
```
```
systemctl list-units --all --state=inactive
```
```
systemctl list-units --type=service
```
```
systemctl list-unit-files
```
## Start/Stop a service - will start/stop it for current session
```
sudo systemctl start foo.service 
```
```
sudo systemctl stop foo.service 
```

## Enable/Disable a service - will set service to start/not-start at startup
```
sudo systemctl enable foo.service 
```
```
sudo systemctl disable foo.service 
```
## Soft Reload
```
sudo systemctl daemon-reload
```
## Check status
```
systemctl status foo.service
```

## Check certain cases
```
systemctl is-active application.service
```
```
systemctl is-enabled application.service
```
```
systemctl is-failed application.service
```

## Dependencies
```
systemctl list-dependencies foo.service
```

## Service Details
```
systemctl show foo.service
```