# Systemd

This section contains info relating to systemd services

Systemd services are stored in `.service` files in `/etc/systemd/system/`
Like 
```
/etc/systemd/system/sshd.service
```

systemd commands are usually in the form
```
sudo systemctl <command> foo.service
```
the `.service` is optional and can be omitted