# Systemd Service File Sample

```text
[Unit]
Description=Terminal Alpha Beta Service
After=network.target

[Service]
User=root
WorkingDirectory=/home/pi/Dev/terminal_alpha_beta
ExecStart=/home/pi/Dev/terminal_alpha_beta/terminal_alpha_beta
Restart=always

[Install]
WantedBy=multi-user.target
```

| Property    | Explanation      |  Possible Values |
| ------- | ------ | ------ |
| `WorkingDirectory` | Directory to run the program in  | _________ |
| `ExecStart` | Command to execute | _________ |
| `Restart` | When to restart | ToDo |
