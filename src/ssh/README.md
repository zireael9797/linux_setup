# SSH

## Config file
The ssh config file is `/etc/ssh/sshd_config`
You can change various things there like the port
Just uncomment any line you want to override and set the value
```
...
# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Port 12345
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
...
```

then you need to restart the `ssh.service` (notice it's ssh, not sshd)
```
sudo systemctl restart ssh.service
```

Note: On the machine you are using you might need to run `ssh-keygen -R [ras.pi.ip.address]:sshport` if you change the port