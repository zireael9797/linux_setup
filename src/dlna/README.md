# DLNA Server
This section will set up a DLNA server for media sharing

## Install
```
sudo apt install minidlna
```

## Add Media folders to config
- Open config file
    ```
    sudo vim /etc/minidlna.conf
    ```
- Add `media_dir` entries in the correct area  
  __Note: You can append media type like Audio, Picture, Video as `media_dir=V,/Share`__
    ```
    media_dir=/Share
    ```
- Find the friendly_name property, uncomment and set it
    ```
    friendly_name=Share
    ```
## Restart
```
sudo systemctl restart minidlna
```
## Stats
You can see DLNA stats at http://__DeviceIp__:8200