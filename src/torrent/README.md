# Torrent

## Qbittorrent installation
install qbittorrent and qbittorrent-nox
```
sudo apt-get install qbittorrent qbittorrent-nox
```
## Enable WebUI
- Go to settings
- Enable webui
- Set a port for the webui
- Set login id and password

## Config file
qBittorrent config files are stored in

`~/.config/qBittorrent/qBittorrent.conf`