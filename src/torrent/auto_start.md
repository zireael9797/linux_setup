# Auto Start

Create file `qbittorrent.service`
```
sudo vim /etc/systemd/system/qbittorrent.service
```

Find `mount` to wait for
```bash
systemctl list-units --type=mount
```
Will result in something like
```text
UNIT                          LOAD   ACTIVE SUB     DESCRIPTION                     
-.mount                       loaded active mounted /                               
boot.mount                    loaded active mounted /boot                           
dev-mqueue.mount              loaded active mounted POSIX Message Queue File System      
Share.mount          loaded active mounted /Share              
```

Content of service  
_Note: replace `username` with yours, don't use `root` as it may cause issues_
_Note: Requires and After are used to ensure it waits for those mounts_
```
[Unit]
Description=qBittorrent Daemon Service
Documentation=man:qbittorrent-nox(1)
Wants=network-online.target
Requires=Share.mount
After=network-online.target nss-lookup.target Share.mount
 
[Service]
Type=exec
User=<username>
ExecStart=/usr/bin/qbittorrent-nox
TimeoutStopSec=infinity
Restart=always
RestartSec=10
 
[Install]
WantedBy=multi-user.target
```

Reload daemon, Start service, Set to auto-start
```
sudo systemctl daemon-reload
sudo systemctl start qbittorrent.service
sudo systemctl enable qbittorrent.service
```

Check status
```
sudo systemctl status qbittorrent.service
```