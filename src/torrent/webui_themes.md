# WebUI Themes

It's possible to setup custom WebUI themes like [VueTorrent](https://github.com/WDaan/VueTorrent)

Download and extract theme to any folder like
`/usr/share/qBittorrent/vuetorrent`

open config 
```
sudo vim ~/.config/qBittorrent/qBittorrent.conf
```

enable alternate web ui and set it's path

```diff
...
State\size=@Size(779 591)
WebUI\Address=*
- WebUI\AlternativeUIEnabled=false
+ WebUI\AlternativeUIEnabled=true
WebUI\AuthSubnetWhitelist=192.168.0.0/24
WebUI\AuthSubnetWhitelistEnabled=true

...

WebUI\Port=1234
WebUI\ReverseProxySupportEnabled=false
- WebUI\RootFolder=
+ WebUI\RootFolder=/usr/share/qBittorrent/vuetorrent
WebUI\SecureCookie=true
WebUI\ServerDomains=*
...
```