# Alternative Qbittorrent Nox

The qBittorrent in your repo may be old.

You can aquire a newer version at [qbittorrent-nox-static](https://github.com/userdocs/qbittorrent-nox-static)

Find and get the latest release binary from the _Latest Releases_ section

Put it in `/usr/bin/`, where most binaries including `qbittorrent-nox` are

## (Optional) Update the auto start service

replace `ExecStart=/usr/bin/qbittorrent-nox` with path to your alternative binary

```diff
[Unit]
Description=qBittorrent Daemon Service
Documentation=man:qbittorrent-nox(1)
Wants=network-online.target
Requires=Share.mount
After=network-online.target nss-lookup.target Share.mount
 
[Service]
Type=exec
User=<username>
- ExecStart=/usr/bin/qbittorrent-nox
+ ExecStart=/usr/bin/armv7-qbittorrent-nox
TimeoutStopSec=infinity
Restart=always
RestartSec=10
 
[Install]
WantedBy=multi-user.target
```