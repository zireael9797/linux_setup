# Auto Mount

## We will add the drive to `fstab`
## Open the fstab file
```
sudo vim etc/fstab
```
## Append 
```
UUID=05ac0341-38f1-489b-b054-4a4f811d1537 /Share ext4 defaults,nofail 0 2
```
to the end
```
...
PARTUUID=9e3452a3-01  /boot           vfat    defaults          0       2
PARTUUID=9e3452a3-02  /               ext4    defaults,noatime  0       1
# a swapfile is not a swap partition, no line here
#   use  dphys-swapfile swap[on|off]  for that
UUID=05ac0341-38f1-489b-b054-4a4f811d1537 /Share ext4 defaults,nofail 0 2
```
