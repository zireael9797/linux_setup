# Change Mount Point

## Unmount
```
sudo umount /media/pi/Transcend
```

## Create a new folder to Mount at
- make directory
    ```
    sudo mkdir /Share
    sudo chown root:root /Share
    sudo chmod 0777 /Share
    ```
- finally run `ls` to be sure
    ```
    ls /Share
    ```
## Mount
```
sudo mount /dev/sda1 /Share
```