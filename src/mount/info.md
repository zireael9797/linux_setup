# Getting Info

## List connected drives
```shell
lsblk
```
will output
```
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda           8:0    0 232.9G  0 disk 
└─sda1        8:1    0 232.9G  0 part /media/pi/Transcend
mmcblk0     179:0    0  29.2G  0 disk 
├─mmcblk0p1 179:1    0   256M  0 part /boot
└─mmcblk0p2 179:2    0  28.9G  0 part /
```

## Getting UUID
```shell
sudo blkid
```
will output
```
/dev/mmcblk0p1: LABEL_FATBOOT="boot" LABEL="boot" UUID="8D64-2011" TYPE="vfat" PARTUUID="9e3452a3-01"
/dev/mmcblk0p2: LABEL="rootfs" UUID="3857a514-b0f4-49ce-8430-34762068bb6f" TYPE="ext4" PARTUUID="9e3452a3-02"
/dev/mmcblk0: PTUUID="9e3452a3" PTTYPE="dos"
/dev/sda1: LABEL="Transcend" UUID="05ac0341-38f1-489b-b054-4a4f811d1537" TYPE="ext4" PARTUUID="1d55f460-02"
```
Copy the UUID: `05ac0341-38f1-489b-b054-4a4f811d1537` for the next step