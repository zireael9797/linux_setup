# Adding Filesystem Support

Out of the box your linux distro may not support all filesystems, or may only support them as readonly.
But adding support is easy enough
- NTFS
  ```
  sudo apt-get install ntfs-3g
  ```
- exFAT
  ```
  sudo apt-get install exfat-fuse exfat-utils
  ```