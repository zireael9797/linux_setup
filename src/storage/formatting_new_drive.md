# Formatting New Drive

When formatting a new out of packaging Drive

Select the drive using `parted`
```shell
sudo parted /dev/sdx
```

Initiate the filesystem
```shell
mklabel msdos
```

Now quit `parted` with `q` and then reboot

After rebooting, select the drive again
```shell
sudo parted /dev/sdx
```

Make a new `ext4` partition, set it to cover the entire drive. It'll have a name like `sdx1`
```shell
mkpart primary ext4 0% 100%
```
Now quit `parted` with `q`

__Note: exFAT has file speed and size issues on linux__
Now convert partition to exFAT using `mkfs.exfat` if you want
```shell
sudo mkfs.exfat /dev/sdx1
```

## This section is faulty and needs further fixes
Issues regarding alignment on exfat, need to investigate GPT, need to investigate Windows support
Until then formatting from windows is better

Hints

Goes after the mkpart line?
```shell
sudo parted /dev/sdx set 1 msftdata on
```

```shell
sudo parted /dev/sdx mklabel gpt
sudo parted -a optimal /dev/sdx/ mkpart primary '0%' '100%'
sudo mkfs.exfat /dev/sdx1
```